window.onload = () => {
  setTimeout(() => {
    document.getElementById("loading-screen").style.display = "none";
  }, 500); //wait for page load PLUS two seconds.
};
window.onbeforeunload = function () {
  window.scrollTo(0, 0);
};
