const langfile = require("../lang/lang.json");
const getAllDom = document.querySelectorAll("[data-lang]");
let lang;
getLang();

function getLang() {
  if (localStorage.getItem("language")) {
    lang = localStorage.getItem("language");
    if (lang == "ar") {
      setDir("rtl");
    } else {
      setDir("ltr");
    }
  } else {
    localStorage.setItem("language", "en");
    lang = localStorage.getItem("language");
    setDir("ltr");
  }
  addWords();
}

function langCahnge(newlang) {
  localStorage.setItem("language", newlang);
  lang = newlang;
}
function setDir(dir) {
  let getHtml = document.querySelector("html");
  getHtml.setAttribute("lang", lang);
  getHtml.setAttribute("dir", dir);
  if (lang == "ar") {
    document.documentElement.style.setProperty('--right', 'left');
  document.documentElement.style.setProperty('--left', 'right');
  } else {
    document.documentElement.style.setProperty('--right', 'right');
  document.documentElement.style.setProperty('--left', 'left');
  }
}

document.getElementById("lang-btn").onclick = () => {
  if (lang == "ar") {
    langCahnge("en");
    setDir("ltr");
  } else {
    langCahnge("ar");
    setDir("rtl");
  }
  addWords();
};
function addWords() {
  getAllDom.forEach((element) => {
    try {
      element.innerHTML = langfile[element.getAttribute("data-lang")][lang];
    } catch (err) {
      console.log(err.message);
    }
  });
}
