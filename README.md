# Global Vision Medical WebSite

this was a pug.js & Sass template 

![Preview Img](ux/screencapture.png)

https://global-vision-medical.mohamed-ashamallah.com/

## Quickstart

1. Install the [node.js](https://nodejs.org/en/)
2. Clone the project

    ```bash
    git clone https://Power50015@bitbucket.org/Power50015/global-vision-medical.git
    ```

3. Go to project folder and run

    ```bash
    npm install
    ```

4. Start development mode

    ```bash
    npm run dev
    ```

5. In browser open page with address [http://localhost:8000/](http://localhost:8000/)

### Main tasks

- npm run dev -  launches watchers and server & compile project.
- npm run build - optimize & minify files for production version.

## INTRODUCTION & FEATURE

Creative & Modern site is a perfect template for Business Startups, web
studio and creative agencies. This is multi page for placing your
information. All files and code has been well organized and nicely commented for easy to customize.


## MAIN FEATURES :

- Valid HTML5, CSS3.
- W3C Validated.
- Pug, Sass, JS.
- Fully Customizable.
- Clean Code.
- Fully Responsive.
- Font-awesome-5.


## FILES INCLUDED :

- Pug Files.
- SCSS Files.
- JS Files.

## Credits

- Google Fonts 
- Font Awesome
- Webpack

## Support:

- If you need any help using the file or need special customizing please contact me via my bitbucket or my Website.
- If you like my html template, please follwo me , We’ll appreciate it very much Thank you.
